#include <QString>
#include <QVersionNumber>
const QVersionNumber VERSION = QVersionNumber::fromString(QString(APP_VERSION_MACRO));
