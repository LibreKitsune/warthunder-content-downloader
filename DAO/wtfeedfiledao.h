#ifndef WTFEEDFILEDAO_H
#define WTFEEDFILEDAO_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

class WtFeedFileDAO : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedFileDAO(QObject *parent = nullptr);
    ~WtFeedFileDAO();
    void getFile(QUrl url);

public slots:
    void replyFinished(QNetworkReply *reply);

signals:
    void responseReceived(QByteArray file, QNetworkReply *reply);

private:
    QNetworkAccessManager *m_manager = nullptr;
    QNetworkReply* m_reply = nullptr;

};

#endif // WTFEEDFILEDAO_H
