#ifndef WTFEEDDAO_H
#define WTFEEDDAO_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QNetworkReply>
#include <QNetworkRequest>
#include "../model/wtfeedunloggedrequest.h"
class WtFeedDAO : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedDAO(QObject *parent = nullptr);
    void getUnlogged(WtFeedUnloggedRequest *request);

public slots:
    void replyFinished(QNetworkReply *reply);

private:
    const QString m_baseurl = "https://live.warthunder.com/api/feed/get_regular/";
    QNetworkAccessManager* m_manager;
    QHttpPart createTextPart(QString name, QVariant value);

signals:
    void responseReceived(QByteArray json, QNetworkReply *reply);
};

#endif // WTFEEDDAO_H
