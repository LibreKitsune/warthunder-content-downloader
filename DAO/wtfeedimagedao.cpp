#include "wtfeedimagedao.h"

WtFeedImageDAO::WtFeedImageDAO(QObject *parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::finished, this, &WtFeedImageDAO::replyFinished);
}

void WtFeedImageDAO::getImage(QUrl url)
{
    QNetworkRequest request(url);
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);

    m_reply = m_manager->get(request);
}

void WtFeedImageDAO::replyFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        QByteArray data = reply->readAll();
        QPixmap pixmap;
        pixmap.loadFromData(data);

        emit responseReceived(pixmap, reply);
    }
    else
    {
        qWarning() << reply->error();
        qWarning() << reply->errorString();
    }
}

WtFeedImageDAO::~WtFeedImageDAO()
{
    if (m_reply != nullptr)
    {
        m_reply->abort();
    }
}
