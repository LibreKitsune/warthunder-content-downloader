#ifndef WTFEEDIMAGEDAO_H
#define WTFEEDIMAGEDAO_H

#include <QObject>
#include <QPixmap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
class WtFeedImageDAO : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedImageDAO(QObject *parent = nullptr);
    ~WtFeedImageDAO();
    void getImage(QUrl url);

private slots:
    void replyFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager* m_manager = nullptr;
    QNetworkReply* m_reply = nullptr;

signals:
    void responseReceived(QPixmap image, QNetworkReply *reply);
};

#endif // WTFEEDIMAGEDAO_H
