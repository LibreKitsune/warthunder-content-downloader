#include "wtfeedfiledao.h"

WtFeedFileDAO::WtFeedFileDAO(QObject *parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::finished, this, &WtFeedFileDAO::replyFinished);
}

void WtFeedFileDAO::getFile(QUrl url)
{
    QNetworkRequest request(url);
    request.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);
    m_reply = m_manager->get(request);
}

void WtFeedFileDAO::replyFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        QByteArray data = reply->readAll();

        emit responseReceived(data, reply);
    }
    else
    {
        qWarning() << reply->error();
        qWarning() << reply->errorString();
    }
}

WtFeedFileDAO::~WtFeedFileDAO()
{
    if (m_reply != nullptr)
    {
        m_reply->abort();
    }
}
