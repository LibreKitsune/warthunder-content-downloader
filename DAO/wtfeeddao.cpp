#include "wtfeeddao.h"

WtFeedDAO::WtFeedDAO(QObject *parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager, &QNetworkAccessManager::finished, this, &WtFeedDAO::replyFinished);
}

void WtFeedDAO::getUnlogged(WtFeedUnloggedRequest *request)
{
    QUrl url = QUrl(m_baseurl);
    QNetworkRequest qrequest;
    qrequest.setTransferTimeout(QNetworkRequest::DefaultTransferTimeoutConstant);
    qrequest.setUrl(url);

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setParent(m_manager);
    multiPart->append(createTextPart("content",request->content()));
    multiPart->append(createTextPart("sort",request->sort()));
    multiPart->append(createTextPart("user",request->user()));
    if (request->period() > 0)
    {
         multiPart->append(createTextPart("period",request->period()));
    }
    multiPart->append(createTextPart("page",request->page()));
    multiPart->append(createTextPart("featured",request->featured()));
    multiPart->append(createTextPart("subtype",request->subtype()));
    multiPart->append(createTextPart("searchString",request->search()));

    m_manager->post(qrequest, multiPart);
}

void WtFeedDAO::replyFinished(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
    {
        QByteArray data = reply->readAll();

        emit responseReceived(data, reply);
    }
    else
    {
        qWarning() << reply->error();
        qWarning() << reply->errorString();
    }
}

QHttpPart WtFeedDAO::createTextPart(QString name, QVariant value)
{
    QHttpPart part;
    part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"%1\"").arg(name)));
    part.setBody(value.toByteArray());
    return part;
}
