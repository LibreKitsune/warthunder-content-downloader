#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QProcess>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = nullptr);

    bool appDarkTheme();
    bool setupHasBeenRun();
    QString warthunderFolder();
    QString p7zipBin();
    bool writeAppLog();
    bool logstdout();


    void setAppDarkTheme(bool useDark);
    void setSetupHasBeenRun(bool setupComplete);
    void setWarthunderFolder(QString folder);
    void setP7zipBin(QString bin);
    void setWriteAppLog(bool log);
    void setLogstdout(bool log);

signals:

private:
    QSettings m_settings;
    QSettings *m_settings_windows_user = nullptr;
    QSettings *m_settings_windows_machine = nullptr;
    bool windowsDarkTheme();
    QString findSteamFolder();
    QString find7z();
    QString findWarThunder();
    QStringList findSteamLibraryFolders();

    const QString KEY_WINDOWS_USER_THEME = "Microsoft/Windows/CurrentVersion/Themes/Personalize/AppsUseLightTheme";
    const QString KEY_WINDOWS_ALL_7Z = "7-Zip/Path";
    const QString KEY_WINDOWS_USER_STEAM = "Valve/Steam/SteamPath";
    const QString KEY_WINDOWS_MACHINE_STEAM = "WOW6432Node/Valve/Steam/InstallPath";
    const QString KEY_SETUPRUN = "app/setupHasBeenRun";
    const QString KEY_DARKMODE = "app/darkmode";
    const QString KEY_WTPATH = "app/wt-path";
    const QString KEY_7ZPATH = "app/7z-path";
    const QString KEY_WRITELOG = "app/writelog";
    const QString KEY_STDOUTLOG = "app/writestdoutlog";


    const QString PATH_DEFAULT_STEAMAPPS = QString("%1/steamapps");
    const QString PATH_DEFAULT_LIBRARYFOLDERS = QString("%1/steamapps/libraryfolders.vdf");
    const QString PATH_HOME = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    const QString PATH_LINUX_STEAM = QString("%1/.steam/steam").arg(PATH_HOME);
};

#endif // SETTINGS_H
