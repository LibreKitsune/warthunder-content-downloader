#ifndef WTFEEDFILEDOWNLOADER_H
#define WTFEEDFILEDOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDir>
#include <QTemporaryDir>
#include <QFileInfo>
#include <QFile>
#include "../model/DTO/wtfeedpost.h"

class WtFeedFileDownloader : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedFileDownloader(WtFeedPost *post = nullptr, QObject *parent = nullptr);

public slots:
    void startDownload();
    void cancel();

private slots:
    void replyFinished(QNetworkReply *reply);
    void gotNewDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);

signals:
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void finished(QString path, int status);
    void error(QString message);

private:
    WtFeedPost *m_post = nullptr;
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply = nullptr;
    QString m_path;
    QTemporaryDir m_tmp;

};

#endif // WTFEEDFILEDOWNLOADER_H
