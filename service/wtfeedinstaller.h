#ifndef WTFEEDINSTALLER_H
#define WTFEEDINSTALLER_H

#include <QObject>
#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include "../model/DTO/wtfeedpost.h"
#include "../DAO/wtfeedfiledao.h"

class WtFeedInstaller : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedInstaller(QString wtfolder, QString p7zipbin, WtFeedPost *post = nullptr, QObject *parent = nullptr);

public slots:
    void install(QString sourcefile);
    void readyReadStandardOutput();
    void readyReadStandardError();
    void processFinished(int exitCode, QProcess::ExitStatus exitStatus);

signals:
    void progress(int percent);
    void installFinished(int exitCode, bool copiedToDownloads);
    void error(QString message);

private:
    WtFeedPost *m_post = nullptr;
    QString m_target;
    QString m_wtFolder;
    QString m_p7zipBin;
    QString getTargetForType(WtFeedEnums::Content type);
    QProcess *m_zipprocess = nullptr;
    bool needsUnzip(WtFeedEnums::Content type);
    QString m_sourcefile;


};

#endif // WTFEEDINSTALLER_H
