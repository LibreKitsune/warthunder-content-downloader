#include "settings.h"

Settings::Settings(QObject *parent) : QObject(parent)
{
    #ifdef Q_OS_WIN
        m_settings_windows_user = new QSettings("HKEY_CURRENT_USER\\Software\\",QSettings::NativeFormat,this);
        m_settings_windows_machine = new QSettings("HKEY_LOCAL_MACHINE\\Software\\",QSettings::NativeFormat,this);
    #endif
}

bool Settings::appDarkTheme()
{
    bool defaultValue = false;
    #ifdef Q_OS_WIN
        defaultValue = windowsDarkTheme();
    #endif
    return m_settings.value(KEY_DARKMODE,defaultValue).toBool();
}

void Settings::setAppDarkTheme(bool useDark)
{
    m_settings.setValue(KEY_DARKMODE,useDark);
    m_settings.sync();
}

bool Settings::setupHasBeenRun()
{
    return m_settings.value(KEY_SETUPRUN,false).toBool();
}

void Settings::setSetupHasBeenRun(bool setupComplete)
{
    m_settings.setValue(KEY_SETUPRUN,setupComplete);
    m_settings.sync();
}

QString Settings::warthunderFolder()
{
    return m_settings.value(KEY_WTPATH,findWarThunder()).toString();
}

void Settings::setWarthunderFolder(QString folder)
{
    m_settings.setValue(KEY_WTPATH,folder);
    m_settings.sync();
}

QString Settings::p7zipBin()
{
    return m_settings.value(KEY_7ZPATH,find7z()).toString();
}

void Settings::setP7zipBin(QString bin)
{
    m_settings.setValue(KEY_7ZPATH,bin);
    m_settings.sync();
}

bool Settings::windowsDarkTheme()
{
    return !m_settings_windows_user->value(KEY_WINDOWS_USER_THEME,true).toBool();
}

QString Settings::findSteamFolder()
{
    QString path = "";
    qInfo() << "Find steam";
    #ifdef Q_OS_WIN
        // try to find steam in windows registry

        path = m_settings_windows_user->value(KEY_WINDOWS_USER_STEAM,"").toString();
        qInfo() << "USER" <<path;

        // Check if the user has steam installed
        QDir tmp(path);
        qInfo() << path << tmp.exists() << path.length();
        if (!tmp.exists() || path.length() == 0)
        {
            // Check is steam exists on the machine
            path = m_settings_windows_machine->value(KEY_WINDOWS_MACHINE_STEAM,"").toString();
            qInfo() << "MACHINE" << path;
        }
    #endif

    #ifdef Q_OS_LINUX
        // use linux default path
        path = PATH_LINUX_STEAM;
    #endif
    QDir dir(path);
    if (dir.exists())
    {
        return dir.absolutePath();
    }
    else
    {
        return "";
    }
}

QString Settings::find7z()
{
    QString file = "%1/%2";
    qInfo() << "Find 7zip";
    #ifdef Q_OS_WIN
        // Try to find 7z in windows registry

        file = file.arg(m_settings_windows_user->value(KEY_WINDOWS_ALL_7Z,"").toString(),"7z.exe");
        qInfo() << "USER" << file;

        // Check if the user has 7zip
        QFileInfo tmp(file);
        qInfo() << file << tmp.exists() << tmp.isFile() << file.length();
        if (!tmp.exists() || !tmp.isFile() || file.length() == 0)
        {
           file = "%1/%2";
            // Check if 7zip exists on the machine

           file = file.arg(m_settings_windows_machine->value(KEY_WINDOWS_ALL_7Z,"").toString(),"7z.exe");
           qInfo() << "MACHINE" << file;
        }

    #endif
    #ifdef Q_OS_LINUX
        // Try to find "7z" via "which"

        // Prepare command parameters
        int timeout_milliseconds = 1000;
        QString command = "which";
        QStringList args;
        args << "7z";

        // Start the command
        QProcess which;

        which.start(command,args);
        bool success = which.waitForFinished(timeout_milliseconds);
        qInfo() << "Executing \"which\"" << command << args << success;
        if (success)
        {
            // get output of the command
            QString whichOutput = which.readAllStandardOutput();
            file = whichOutput.trimmed();
            qInfo() << "\"which\" output" << file;
        }

    #endif

    QFileInfo fileinfo(file);
    if (fileinfo.exists())
    {
        return fileinfo.absoluteFilePath();
    }
    else
    {
        return "";
    }
}

QStringList Settings::findSteamLibraryFolders()
{
    QStringList folders;

    // Add the default library folder that always exists
    QString steamfolder = findSteamFolder();
    QString defaultfolder = PATH_DEFAULT_STEAMAPPS.arg(steamfolder);
    folders.append(defaultfolder);

    QString librarySettigns = PATH_DEFAULT_LIBRARYFOLDERS.arg(findSteamFolder());
    QFile file(librarySettigns);

    if (file.open(QFile::ReadOnly))
    {
        // read the library settings
        QByteArray data = file.readAll();
        QString dataString(data);

        // Iterate over each line
        foreach(QString string, dataString.split("\n"))
        {
            // check if the line contains a "path" key
            if (string.contains("path"))
            {
                // Split the line in key and value
                QStringList kv = string.split("\t\t");
                if (kv.count() == 3)
                {
                    // 0 == whitespace
                    // 1 == key
                    // 2 == value
                    folders.append(PATH_DEFAULT_STEAMAPPS.arg(kv.at(2)).replace("\"",""));
                }
            }
        }
        file.close();
    }

    return folders;
}

QString Settings::findWarThunder()
{
    foreach(QString folder, findSteamLibraryFolders())
    {
        QString warthunder = QString("%1/common/War Thunder").arg(folder);

        QDir wt(warthunder);
        qInfo() << "check" << warthunder << wt.exists();
        if (wt.exists())
        {
            return wt.absolutePath();
        }
    }
    return "";
}

bool Settings::writeAppLog()
{
    return m_settings.value(KEY_WRITELOG,false).toBool();
}

void Settings::setWriteAppLog(bool log)
{
    m_settings.setValue(KEY_WRITELOG,log);
    m_settings.sync();
}

bool Settings::logstdout()
{
    return m_settings.value(KEY_STDOUTLOG,false).toBool();
}

void Settings::setLogstdout(bool log)
{
    m_settings.setValue(KEY_STDOUTLOG,log);
    m_settings.sync();
}
