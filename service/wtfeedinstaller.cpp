#include "wtfeedinstaller.h"

WtFeedInstaller::WtFeedInstaller(QString wtfolder, QString p7zipbin, WtFeedPost *post, QObject *parent) : QObject(parent), m_post(post), m_wtFolder(wtfolder), m_p7zipBin(p7zipbin)
{
    if (m_post != nullptr)
    {
        m_target = getTargetForType(m_post->type());
    }

}

QString WtFeedInstaller::getTargetForType(WtFeedEnums::Content type)
{
    switch(type)
    {
        case WtFeedEnums::all:
            return QString("%1/liveWarthunderCom/mixed").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
        case WtFeedEnums::image:
            return QString("%1/liveWarthunderCom/images").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
        case WtFeedEnums::video:
            return QString("%1/liveWarthunderCom/videos").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
        case WtFeedEnums::camouflage:
            return QString("%1/UserSkins").arg(m_wtFolder);
        break;
        case WtFeedEnums::sight:
            return QString("%1/UserSights").arg(m_wtFolder);
        break;
        case WtFeedEnums::mission:
            return QString("%1/UserMissions").arg(m_wtFolder);
        break;
        case WtFeedEnums::model:
            return QString("%1/liveWarthunderCom/models").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
        case WtFeedEnums::location:
            return QString("%1/liveWarthunderCom/locations").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
        case WtFeedEnums::sound:
            return QString("%1/liveWarthunderCom/sounds").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        break;
    }

    return QString("%1/liveWarthunderCom/mixed").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
}

bool WtFeedInstaller::needsUnzip(WtFeedEnums::Content type)
{
    switch(type)
    {
        case WtFeedEnums::all:
            return false;
        break;
        case WtFeedEnums::image:
            return false;
        break;
        case WtFeedEnums::video:
            return false;
        break;
        case WtFeedEnums::camouflage:
            return true;
        break;
        case WtFeedEnums::sight:
            return true;
        break;
        case WtFeedEnums::mission:
            return true;
        break;
        case WtFeedEnums::model:
            return true;
        break;
        case WtFeedEnums::location:
            return true;
        break;
        case WtFeedEnums::sound:
            return true;
        break;
    }
    return false;
}

void WtFeedInstaller::readyReadStandardOutput()
{
    QString output = QString(m_zipprocess->readAllStandardOutput());
    qInfo() << output;
    output = output.replace('\b',"").replace(" ","");
    int index = output.indexOf('%');

    bool ok;

    int percent = output.left(index).toInt(&ok);

    if (ok)
    {
        emit progress(percent);
    }

}

void WtFeedInstaller::readyReadStandardError()
{
    QString output = QString(m_zipprocess->readAllStandardError());

    emit error(output);
}

void WtFeedInstaller::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    if (exitStatus == QProcess::NormalExit)
    {
        #ifdef Q_OS_LINUX
            QDir sourcepath("");
            sourcepath.remove(m_sourcefile);

        #elif defined(Q_OS_CYGWIN)
            #error "We don't support that version yet..."
        #else
            //WINDOWS 10
            QDir sourcepath("C:");
            sourcepath.remove(m_sourcefile);
        #endif
    }
    emit installFinished(exitCode,false);
}

void WtFeedInstaller::install(QString sourcefile)
{
    m_sourcefile = sourcefile;
    // Get the filename of the file
    QFileInfo fi(sourcefile);
    QString fileName = fi.fileName();

    QString fileNoExtension = fileName.left(fileName.lastIndexOf("."));
    QString targetFolder = QString("%1/%2").arg(m_target,fileNoExtension);

    // Create target folder if it doesnt exist
    if (m_target.contains("liveWarthunderCom"))
    {
        QDir baseDir;
        QString baseTarget = QString("%1/liveWarthunderCom").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
        qInfo() << "Creating folder" << baseTarget << baseDir.mkdir(baseTarget);
    }

    QDir dir;
    qInfo() << "Creating folder" << m_target << dir.mkdir(m_target);




    if (needsUnzip(m_post->type()))
    {
        if (m_zipprocess != nullptr)
        {
            delete m_zipprocess;
            m_zipprocess = nullptr;
        }

        if (m_zipprocess == nullptr)
        {
            QDir targetdir(targetFolder);
            if (targetdir.exists())
            {
                qInfo() << "Deleting" << targetFolder << targetdir.removeRecursively();
            }


            QStringList args;
            // -bs## redirect only progress to stdout everyting else to stderr
            args << "x" << sourcefile << "-bso0" << "-bsp1" << "-bse2" << QString("-o%1").arg(targetFolder) << "-y";

            qInfo() << "Running" << m_p7zipBin << args;

            m_zipprocess = new QProcess(this);
            m_zipprocess->start(m_p7zipBin,args);

            connect(m_zipprocess, &QProcess::readyReadStandardOutput, this, &WtFeedInstaller::readyReadStandardOutput);
            connect(m_zipprocess, &QProcess::readyReadStandardError, this, &WtFeedInstaller::readyReadStandardError);
            connect(m_zipprocess, SIGNAL(finished(int,QProcess::ExitStatus)), this,SLOT(processFinished(int,QProcess::ExitStatus)));
        }
    }
    else
    {
        // Create absolute targetfile name
        QString targetfile = QString("%1/%2").arg(m_target, fileName);

        qDebug() << sourcefile << targetfile;

        // Test to copy the file
        QFile::copy(sourcefile, targetfile);
        emit installFinished(0,true);
    }
}
