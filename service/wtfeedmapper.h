#ifndef WTFEEDMAPPER_H
#define WTFEEDMAPPER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMimeType>
#include <QMimeDatabase>
#include "../model/DTO/wtfeedauthor.h"
#include "../model/DTO/wtfeedfile.h"
#include "../model/DTO/wtfeedimage.h"
#include "../model/DTO/wtfeedpost.h"
#include "../model/DTO/wtfeedresponse.h"
#include "../model/wtfeedenums.h"

class WtFeedMapper : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedMapper(QObject *parent = nullptr);

    WtFeedAuthor* createAuthorFromJSON(QByteArray data);
    WtFeedFile* createFileFromJSON(QByteArray data);
    WtFeedImage* createImageFromJSON(QByteArray data);
    WtFeedPost* createPostFromJSON(QByteArray data);
    WtFeedResponse* createResponseFromJSON(QByteArray data);

private:
    QMimeDatabase m_mimeDb;

signals:

};

#endif // WTFEEDMAPPER_H
