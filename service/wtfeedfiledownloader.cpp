#include "wtfeedfiledownloader.h"

WtFeedFileDownloader::WtFeedFileDownloader(WtFeedPost* post, QObject *parent) :
    QObject(parent),
    m_post(post)
{
    m_manager = new QNetworkAccessManager(this);

    connect(m_manager, &QNetworkAccessManager::finished, this, &WtFeedFileDownloader::replyFinished);

    if (m_post != nullptr)
    {
        if (m_post->file() != nullptr)
        {
            m_path = QString("%1/%2").arg(m_tmp.path(),m_post->file()->name());
        }
        else
        {
            if (m_post->images().count() > 0)
            {
                QString nameFromSrc = m_post->images().at(0)->src().toString();
                QFileInfo fi(nameFromSrc);
                nameFromSrc = fi.fileName();
                m_path = QString("%1/%2").arg(m_tmp.path(),nameFromSrc);
            }
        }
        qInfo() << "download target for post" << m_path;
    }
}

void WtFeedFileDownloader::startDownload()
{
    if (m_post != nullptr)
    {
        QUrl url;

        if (m_post->type() == WtFeedEnums::image)
        {
            if (m_post->images().count() > 0)
            {
                url = m_post->images().at(0)->src();
            }
        }
        else
        {
            if (m_post->file() != nullptr)
            {
                url = m_post->file()->link();
            }
        }

        if (url.isValid())
        {
            QNetworkRequest request;
            request.setUrl(url);
            request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
            m_reply = m_manager->get(request);
            connect(m_reply, &QNetworkReply::downloadProgress, this, &WtFeedFileDownloader::gotNewDownloadProgress);
        }
    }
}

void WtFeedFileDownloader::gotNewDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    emit downloadProgress(bytesReceived,bytesTotal);
}

void WtFeedFileDownloader::replyFinished(QNetworkReply *reply)
{
    Q_UNUSED(reply);
    bool ok = false;
    int statuscode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(&ok);
    if (!ok || statuscode == 0)
    {
        statuscode = 500;
    }

    if (reply->error() == QNetworkReply::NoError && statuscode == 200)
    {



        QByteArray data = reply->readAll();

        QString filename = m_path;

        qInfo() << "saving file" << filename;
        QFile file(filename);
        if (file.open(QIODevice::ReadWrite))
        {
            file.write(data);
            file.close();
        }
        else
        {
            qWarning() << filename << file.error() << file.errorString();
            emit error(QString("Message: %1 Error: %2").arg(filename,file.errorString()));
            statuscode = 500;
        }
    }
    else
    {
        statuscode = 500;
        qWarning() << reply->error() << reply->errorString() << reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    }

    emit finished(m_path, statuscode);
}

void WtFeedFileDownloader::cancel()
{
    if (m_reply != nullptr)
    {
        m_reply->abort();
    }
}
