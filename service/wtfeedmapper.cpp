#include "wtfeedmapper.h"

WtFeedMapper::WtFeedMapper(QObject *parent) : QObject(parent)
{

}

WtFeedAuthor* WtFeedMapper::createAuthorFromJSON(QByteArray data)
{
    WtFeedAuthor* author = nullptr;
    QJsonDocument json = QJsonDocument::fromJson(data);

    if (json.isObject())
    {
        QJsonObject obj = json.object();

        author = new WtFeedAuthor();

        if (obj.contains("id"))
        {
            author->setID(obj["id"].toInt(0));
        }

        if (obj.contains("nickname"))
        {
            author->setNickname(obj["nickname"].toString(""));
        }

        if (obj.contains("avatar"))
        {
            QString avatar = obj["avatar"].toString("");
            QUrl url = QUrl(avatar);
            if (url.isValid())
            {
                author->setAvatarUrl(url);
            }
        }
    }

    return author;
}

WtFeedFile* WtFeedMapper::createFileFromJSON(QByteArray data)
{
    WtFeedFile* file = nullptr;
    QJsonDocument json = QJsonDocument::fromJson(data);

    if (json.isObject())
    {
        QJsonObject obj = json.object();

        file = new WtFeedFile();

        if (obj.contains("id"))
        {
            file->setID(obj["id"].toInt(0));
        }

        if (obj.contains("name"))
        {
            file->setName(obj["name"].toString(""));
        }

        if (obj.contains("link"))
        {
            QString link = obj["link"].toString("");
            QUrl url = QUrl(link);
            if (url.isValid())
            {
                file->setLink(url);
            }
        }

        if (obj.contains("type"))
        {
            QString type = obj["type"].toString("");

            QMimeType mimetype = m_mimeDb.mimeTypeForName(type);
            if (mimetype.isValid())
            {
                file->setType(mimetype);
            }
        }

        if (obj.contains("size"))
        {
            file->setSize(obj["size"].toInt(0));
        }

    }

    return file;
}

WtFeedImage* WtFeedMapper::createImageFromJSON(QByteArray data)
{
    WtFeedImage* image = nullptr;
    QJsonDocument json = QJsonDocument::fromJson(data);

    if (json.isObject())
    {
        QJsonObject obj = json.object();

        image = new WtFeedImage();

        if (obj.contains("id"))
        {
            image->setID(obj["id"].toInt(0));
        }

        if (obj.contains("type"))
        {
            QString type = obj["type"].toString("");

            QMimeType mimetype = m_mimeDb.mimeTypeForName(type);
            if (mimetype.isValid())
            {
                image->setType(mimetype);
            }
        }

        if (obj.contains("src"))
        {
            QString link = obj["src"].toString("");
            QUrl url = QUrl(link);
            if (url.isValid())
            {
                image->setSrc(url);
            }
        }

        if (obj.contains("width") && obj.contains("height"))
        {

            QSize size(obj["width"].toInt(0), obj["height"].toInt(0));
            if (size.isValid())
            {
                image->setSize(size);
            }
        }

        if (obj.contains("ratio"))
        {
            image->setRatio(obj["ratio"].toDouble(0));
        }
    }

    return image;
}

WtFeedPost* WtFeedMapper::createPostFromJSON(QByteArray data)
{
    WtFeedPost* post = nullptr;
    QJsonDocument json = QJsonDocument::fromJson(data);

    if (json.isObject())
    {
        QJsonObject obj = json.object();

        post = new WtFeedPost();

        if (obj.contains("id"))
        {
            post->setID(obj["id"].toInt(0));
        }

        if (obj.contains("author"))
        {
            if (obj["author"].isObject())
            {
                QJsonDocument authorDoc = QJsonDocument(obj["author"].toObject());
                WtFeedAuthor* author = createAuthorFromJSON(authorDoc.toJson());
                if (author != nullptr)
                {
                    post->setAuthor(author);
                }
            }
        }        

        if (obj.contains("images"))
        {
            if (obj["images"].isArray())
            {
                QJsonArray arr = obj["images"].toArray();
                foreach ( const QJsonValue &item , arr)
                {
                    if (item.isObject())
                    {
                        QJsonDocument imageDoc = QJsonDocument(item.toObject());
                        WtFeedImage* img = createImageFromJSON(imageDoc.toJson());

                        if (img != nullptr)
                        {
                            post->addImage(img);
                        }
                    }
                }
            }
        }

        if (obj.contains("file"))
        {
            if (obj["file"].isObject())
            {
                QJsonDocument fileDoc = QJsonDocument(obj["file"].toObject());
                WtFeedFile* file = createFileFromJSON(fileDoc.toJson());                
                if (file != nullptr)
                {
                    post->setFile(file);
                }
            }
        }       

        if (obj.contains("lang_group"))
        {
            post->setLangGroup(obj["lang_group"].toInt(0));
        }

        if (obj.contains("language"))
        {
            post->setLanguage(obj["language"].toString(""));
        }

        if (obj.contains("type"))
        {
            WtFeedEnums::Content type = WtFeedEnums::stringToContent(obj["type"].toString(""));
            post->setType(type);
        }

        if (obj.contains("created"))
        {
            int createdSec = obj["created"].toInt(0);
            QDateTime dt = QDateTime::fromSecsSinceEpoch(createdSec);
            post->setCreated(dt);
        }

        if (obj.contains("isAuthor"))
        {
            post->setIsAuthor(obj["isAuthor"].toBool(false));
        }

        if (obj.contains("visible"))
        {
            post->setVisible(obj["visible"].toBool(true));
        }

        if (obj.contains("isSpecial"))
        {
            post->setIsSpecial(obj["isSpecial"].toBool(false));
        }

        if (obj.contains("likes"))
        {
            post->setLikes(obj["likes"].toInt(0));
        }

        if (obj.contains("isLiked"))
        {
            post->setIsLiked(obj["isLiked"].toBool(false));
        }

        if (obj.contains("views"))
        {
            post->setViews(obj["views"].toInt(0));
        }

        if (obj.contains("doubt"))
        {
            post->setDoubt(obj["doubt"].toBool(false));
        }

        if (obj.contains("featured"))
        {
            post->setFeatured(obj["featured"].toBool(false));
        }

        if (obj.contains("downloads"))
        {
            post->setDownloads(obj["downloads"].toInt(0));
        }

        if (obj.contains("comments"))
        {
            post->setComments(obj["comments"].toInt(0));
        }

        if (obj.contains("isPinned"))
        {
            post->setIsPinned(obj["isPinned"].toBool(false));
        }

        if (obj.contains("isMarketSuitable"))
        {
            post->setIsMarketSuitable(obj["isMarketSuitable"].toBool(false));
        }

        if (obj.contains("canDelete"))
        {
            post->setCanDelete(obj["canDelete"].toBool(false));
        }

        if (obj.contains("canEdit"))
        {
            post->setCanEdit(obj["canEdit"].toBool(false));
        }

        if (obj.contains("description"))
        {
            post->setDescription(obj["description"].toString(""));
        }

        if (obj.contains("pbr_ready"))
        {
            post->setPbrReady(obj["pbr_ready"].toBool(false));
        }
    }

    return post;
}

WtFeedResponse* WtFeedMapper::createResponseFromJSON(QByteArray data)
{
    WtFeedResponse* response = nullptr;
    QJsonDocument json = QJsonDocument::fromJson(data);

    if (json.isObject())
    {
        QJsonObject obj = json.object();

        response = new WtFeedResponse();

        if (obj.contains("status"))
        {
            response->setStatus(obj["status"].toString(""));
        }

        if (obj.contains("data"))
        {
            if (obj["data"].isObject())
            {
                QJsonObject data = obj["data"].toObject();

                if (data.contains("list"))
                {
                    if (data["list"].isArray())
                    {
                        QJsonArray arr = data["list"].toArray();

                        foreach ( const QJsonValue &item , arr)
                        {
                            if (item.isObject())
                            {
                                QJsonDocument postDoc = QJsonDocument(item.toObject());
                                WtFeedPost* post = createPostFromJSON(postDoc.toJson());

                                if (post != nullptr)
                                {
                                    response->addPost(post);
                                }
                            }
                        }
                    }
                }

                if (data.contains("pageTitle"))
                {
                    response->setPageTitle(data["pageTitle"].toString(""));
                }

                if (data.contains("link"))
                {
                    QString link = data["link"].toString("");
                    QUrl url = QUrl(link);
                    if (url.isValid())
                    {
                        response->setLink(url);
                    }
                }
            }
        }
    }

    return response;
}
