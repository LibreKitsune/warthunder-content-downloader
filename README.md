# Warthunder-Content-Downloader 

Warthunder-Content-Downloader is a tool for the game WarThunder.
This tool makes installing Skins, Sights and Missions easy.
Note: images, videos, models, locations and sound get "installed" into your Downloads folder.

## Installation

Installation files can be found under the [Releases](https://gitlab.com/LibreKitsune/warthunder-content-downloader/-/releases) section

### Debian
Download the deb and install it via your package manager

```bash
sudo apt install ./wt-content-downloader_${VERSION}.deb
```

### General Linux
Download the .tar.gz and unpack it.
Also install p7zip from your package manager.
You will also need the following QT 5.15.2 Libraries:
libqt5network5,
libqt5gui5,
libqt5widgets5,
libqt5core5a

```bash
tar -xvf ./wt-content-downloader_${VERSION}.tar.gz
```
The executable resides in the "bin" directory.

### Windows
Install [7-Zip](https://www.7-zip.org/).
Download the 7z file and extract it.

### ALL
After you installed/extracted the programm, you can run it.
It should automatically detect both Warthunder and 7zip.
If not you have to supply the paths to "7z" (or 7z.exe on windows) and the "War Thunder" folder (the folder containing launcher.ico).

## Usage

Just run it and start clicking the buttons.

## Building

Make sure you have QT5 libraries installed.
### Debian:
```bash
sudo apt install git cmake extra-cmake-modules build-essential qt5-qmake qtbase5-dev qtbase5-dev qtbase5-private-dev qttools5-dev qttools5-dev-tools
```
```bash
git clone https://gitlab.com/LibreKitsune/warthunder-content-downloader.git
cd warthunder-content-downloader
mkdir build
cd build
cmake ..
make -j12
cpack -G DEB
```

### Fedora
```bash
sudo dnf install git cmake extra-cmake-modules rpmbuild qt5-qtbase-private-devel qt5-qtx11extras-devel qt5-qtwebchannel-devel qt5-qtwebsockets-devel
```
```bash
git clone https://gitlab.com/LibreKitsune/warthunder-content-downloader.git
cd warthunder-content-downloader
mkdir build
cd build
cmake ..
make -j12
cpack -G RPM
```
### General Linux
Make sure Qt is installed either through your package manager or from [Qt](https://www.qt.io/download-qt-installer)
Also make sure that cmake is available.
```bash
git clone https://gitlab.com/LibreKitsune/warthunder-content-downloader.git
cd warthunder-content-downloader
mkdir build
cd build
cmake ..
make -j12
```

### Windows
Good luck setting up the dev environment. You will need it.
Download [Qt](https://www.qt.io/download-qt-installer),
[CMake](https://cmake.org/download/) and
[Git](https://git-scm.com/downloads)
You might also need MSVC. But MinGW should suffice.

```bash
git clone https://gitlab.com/LibreKitsune/warthunder-content-downloader.git
cd warthunder-content-downloader
mkdir build
cd build
cmake ..
make -j12
```

## Notes
Icon is from [Icons8.com](https://icons8.com)

## License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
