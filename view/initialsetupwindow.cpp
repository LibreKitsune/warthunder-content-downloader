#include "initialsetupwindow.h"
#include "ui_initialsetupwindow.h"

InitialSetupWindow::InitialSetupWindow(Settings *settings, bool restartProg, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InitialSetupWindow),
    settings_ptr(settings),
    restart(restartProg)
{
    ui->setupUi(this);

    connect(ui->bb_install,SIGNAL(accepted()),this,SLOT(accepted()));
    connect(ui->bb_install,SIGNAL(rejected()),this,SLOT(rejected()));
    connect(ui->pb_7z,SIGNAL(clicked()),this,SLOT(zipbrowse()));
    connect(ui->pb_wt,SIGNAL(clicked()),this,SLOT(wtbrowse()));
    connect(ui->pb_detect,SIGNAL(clicked()),this,SLOT(redetect()));

    redetect();
}

void InitialSetupWindow::redetect()
{
    QString sevenZPath = settings_ptr->p7zipBin();
    QString wtPath = settings_ptr->warthunderFolder();
    bool darkMode = settings_ptr->appDarkTheme();

    if (sevenZPath.length() == 0)
    {
        #ifdef Q_OS_LINUX
            // Try to find "7z" via "which"

            // Prepare command parameters
            int timeout_milliseconds = 1000;
            QString command = "which";
            QStringList args;
            args << "7z";

            // Start the command
            QProcess which;
            which.start(command,args);
            bool success = which.waitForFinished(timeout_milliseconds);

            if (success)
            {
                // get output of the command
                QString whichOutput = which.readAllStandardOutput();
                sevenZPath = whichOutput.trimmed();
            }
            else
            {
                sevenZPath = "";
            }


        #elif defined(Q_OS_CYGWIN)
            sevenZPath = "";
        #else
            sevenZPath = "";
        #endif
    }


    ui->le_7z->setText(sevenZPath);
    ui->le_wt->setText(wtPath);
    ui->cb_darktheme->setChecked(darkMode);
    ui->cb_logging->setChecked(settings_ptr->writeAppLog());
    ui->cb_stdout->setChecked(settings_ptr->logstdout());
}

InitialSetupWindow::~InitialSetupWindow()
{
    delete ui;
}

void InitialSetupWindow::accepted()
{
    QDir wt(ui->le_wt->text());
    QFileInfo sevenZip(ui->le_7z->text());

    if (wt.exists() && sevenZip.exists() && sevenZip.isFile())
    {
        settings_ptr->setWarthunderFolder(wt.absolutePath());
        settings_ptr->setP7zipBin(sevenZip.absoluteFilePath());
        settings_ptr->setSetupHasBeenRun(true);
        settings_ptr->setAppDarkTheme(ui->cb_darktheme->isChecked());
        settings_ptr->setWriteAppLog(ui->cb_logging->isChecked());
        settings_ptr->setLogstdout(ui->cb_stdout->isChecked());
        if (restart)
        {
            QProcess::startDetached(QApplication::applicationFilePath(),QStringList());
            exit(12);
        }
        else
        {
            emit closed(true);
            close();
        }
    }

    if (!wt.exists())
    {
        QMessageBox::warning(this,"Folder does not exist",QString("The folder %1 does not exist").arg(wt.absolutePath()));
    }

    if (!sevenZip.exists() || !sevenZip.isFile())
    {
        QMessageBox::warning(this,"File does not exist",QString("The file %1 does not exist").arg(sevenZip.absoluteFilePath()));
    }

}

void InitialSetupWindow::rejected()
{
    if (restart)
    {
        exit(0);
    }
    else
    {
        emit closed(false);
        close();
    }
}

void InitialSetupWindow::zipbrowse()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setViewMode(QFileDialog::ViewMode::Detail);

    if (dialog.exec())
    {
        ui->le_7z->setText(dialog.selectedFiles().at(0));
    }
}

void InitialSetupWindow::wtbrowse()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::Option::ShowDirsOnly);

    if (dialog.exec())
    {
        ui->le_wt->setText(dialog.selectedFiles().at(0));
    }
}
