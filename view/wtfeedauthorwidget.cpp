#include "wtfeedauthorwidget.h"
#include "ui_wtfeedauthorwidget.h"

WtFeedAuthorWidget::WtFeedAuthorWidget(WtFeedAuthor *author, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WtFeedAuthorWidget)
{
    ui->setupUi(this);
    m_author = author;
    m_dao = new WtFeedImageDAO(this);

    connect(m_dao, &WtFeedImageDAO::responseReceived, this, &WtFeedAuthorWidget::imageReceived);

    m_dao->getImage(m_author->avatarUrl());
    ui->labelNickname->setText(m_author->nickname());
}

void WtFeedAuthorWidget::imageReceived(QPixmap img)
{
    this->setMinimumHeight(img.height());
    ui->labelAvatar->setMaximumWidth(img.width());
    ui->labelAvatar->setPixmap(img);
}

WtFeedAuthorWidget::~WtFeedAuthorWidget()
{
    delete ui;
}
