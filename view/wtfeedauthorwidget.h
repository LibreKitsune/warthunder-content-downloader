#ifndef WTFEEDAUTHORWIDGET_H
#define WTFEEDAUTHORWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPainterPath>
#include "../model/DTO/wtfeedauthor.h"
#include "../DAO/wtfeedimagedao.h"
namespace Ui {
class WtFeedAuthorWidget;
}

class WtFeedAuthorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WtFeedAuthorWidget(WtFeedAuthor *author,QWidget *parent = nullptr);
    ~WtFeedAuthorWidget();

private:
    Ui::WtFeedAuthorWidget *ui;
    WtFeedAuthor *m_author;
    WtFeedImageDAO *m_dao;

private slots:
    void imageReceived(QPixmap img);
};

#endif // WTFEEDAUTHORWIDGET_H
