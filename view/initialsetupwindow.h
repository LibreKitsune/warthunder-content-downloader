#ifndef INITIALSETUPWINDOW_H
#define INITIALSETUPWINDOW_H

#include <QWidget>
#include <QProcess>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include "service/settings.h"
namespace Ui
{
    class InitialSetupWindow;
}

class InitialSetupWindow : public QWidget
{
    Q_OBJECT

    public:
        explicit InitialSetupWindow(Settings *settings, bool restartProg = false, QWidget *parent = nullptr);
        ~InitialSetupWindow();

    private:
        Ui::InitialSetupWindow *ui;
        Settings *settings_ptr;
        QString zippath;
        QString wtpath;
        bool restart;

    private slots:
        void zipbrowse();
        void wtbrowse();
        void accepted();
        void rejected();
        void redetect();

    signals:
        void closed(bool ok);

};

#endif // INITIALSETUPWINDOW_H
