#ifndef WTFEEDIMAGEWIDGET_H
#define WTFEEDIMAGEWIDGET_H

#include <QWidget>
#include "../model/DTO/wtfeedimage.h"
#include "../DAO/wtfeedimagedao.h"

namespace Ui {
class WtFeedImageWidget;
}

class WtFeedImageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WtFeedImageWidget(WtFeedImage* image, QWidget *parent = nullptr);
    ~WtFeedImageWidget();

private:
    Ui::WtFeedImageWidget *ui;
    WtFeedImage* m_image;
    WtFeedImageDAO* m_dao;

private slots:
    void imageReceived(QPixmap img);
};

#endif // WTFEEDIMAGEWIDGET_H
