#include "wtfeedpostwidget.h"
#include "ui_wtfeedpostwidget.h"
WtFeedPostWidget::WtFeedPostWidget(WtFeedPost *post,Settings *settings, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WtFeedPostWidget),
    m_settings(settings)
{
    ui->setupUi(this);

    // Store the post
    m_post = post;
    m_post->setParent(this);
    // Setup diashow
    m_imageTimer = new QTimer(this);
    m_imageTimer->start(1000);


    m_downloader = new WtFeedFileDownloader(post,this);
    m_installer = new WtFeedInstaller(m_settings->warthunderFolder(),settings->p7zipBin(),m_post,this);


    initUI();
    initConnect();

}

void WtFeedPostWidget::initUI()
{
    // Set post description and type
    ui->labelDescription->setText(m_post->description());
    ui->labelType->setText(QString("Type: %1").arg(WtFeedEnums::contentToKeyString(m_post->type())));

    ui->labelStatus->setVisible(false);
    ui->progressBarDownload->setVisible(false);
    ui->progressBarInstall->setVisible(false);
    ui->buttonCancel->setVisible(false);
    ui->buttonDownload->setVisible(true);

    // Add Author widget
    m_authorwidget = new WtFeedAuthorWidget(m_post->author(),this);
    ui->authorLayout->addWidget(m_authorwidget);

    // Add images from post
    foreach(WtFeedImage *img, m_post->images())
    {
       //test *test = new class::test(this);
       WtFeedImageWidget* imgwdg = new WtFeedImageWidget(img,this);
       ui->stackedWidget->addWidget(imgwdg);
       m_imagewidgets.append(imgwdg);
    }
}

void WtFeedPostWidget::initConnect()
{
    // Connect signals
    connect(ui->buttonDownload, &QPushButton::clicked, this, &WtFeedPostWidget::downloadButtonClicked);
    connect(m_imageTimer, &QTimer::timeout, this, &WtFeedPostWidget::nextImage);
    connect(ui->buttonDownload, &QPushButton::clicked, m_downloader, &WtFeedFileDownloader::startDownload);
    connect(ui->buttonCancel, &QPushButton::clicked, m_downloader, &WtFeedFileDownloader::cancel);
    connect(m_downloader, &WtFeedFileDownloader::downloadProgress, this, &WtFeedPostWidget::downloadProgress);
    connect(m_downloader, &WtFeedFileDownloader::finished, this, &WtFeedPostWidget::downloadFinished);
    connect(m_downloader, &WtFeedFileDownloader::error, this, &WtFeedPostWidget::downloadError);
    connect(m_installer, &WtFeedInstaller::progress, this, &WtFeedPostWidget::installProgress);
    connect(m_installer, &WtFeedInstaller::installFinished, this, &WtFeedPostWidget::installFinished);
    connect(m_installer, &WtFeedInstaller::error, this, &WtFeedPostWidget::installError);
}

void WtFeedPostWidget::nextImage()
{
    int maxIndex = ui->stackedWidget->count() - 1;
    int current = ui->stackedWidget->currentIndex();
    int nextIndex = current + 1;

    // Wrap back to start
    if (nextIndex > maxIndex)
    {
        nextIndex = 0;
    }

    ui->stackedWidget->setCurrentIndex(nextIndex);
}

void WtFeedPostWidget::previousImage()
{
    int maxIndex = ui->stackedWidget->count() - 1;
    int minIndex = 0;
    int current = ui->stackedWidget->currentIndex();
    int nextIndex = current - 1;

    // Wrap back to the end
    if (nextIndex < minIndex)
    {
        nextIndex = maxIndex;
    }

    ui->stackedWidget->setCurrentIndex(nextIndex);
}

void WtFeedPostWidget::downloadButtonClicked()
{
    ui->labelStatus->setVisible(false);
    ui->buttonDownload->setVisible(false);
    ui->progressBarDownload->setVisible(true);
    ui->buttonCancel->setVisible(true);
    emit downloadRequested(m_post);
}

void WtFeedPostWidget::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBarDownload->setMaximum(bytesTotal);
    ui->progressBarDownload->setValue(bytesReceived);
}

void WtFeedPostWidget::installProgress(int progress)
{
    ui->progressBarInstall->setMaximum(100);
    ui->progressBarInstall->setValue(progress);
}

void WtFeedPostWidget::downloadFinished(QString path, int status)
{
    if (status == 200)
    {
        ui->progressBarDownload->setValue(0);
        ui->progressBarInstall->setVisible(true);
        ui->progressBarDownload->setVisible(false);
        ui->buttonDownload->setVisible(false);
        ui->buttonCancel->setVisible(false);

        m_installer->install(path);

    }
    else
    {
        ui->progressBarDownload->setValue(0);
        ui->progressBarInstall->setVisible(false);
        ui->progressBarDownload->setVisible(false);
        ui->buttonDownload->setVisible(true);
        ui->buttonCancel->setVisible(false);
        ui->labelStatus->setText("ABORTED");
        ui->labelStatus->setVisible(true);
    }
}

void WtFeedPostWidget::installError(QString message)
{
    QMessageBox::warning(this,"7-zip error",message);
}

void WtFeedPostWidget::downloadError(QString message)
{
    QMessageBox::warning(this,"Download error",message);
}

void WtFeedPostWidget::installFinished(int exitcode, bool copied)
{
    if (exitcode == 0)
    {
        if (!copied)
        {
            ui->labelStatus->setText("INSTALLED");
        }
        else
        {
            ui->labelStatus->setText("SAVED TO DOWNLOADS");
        }

    }
    else
    {
        ui->labelStatus->setText(QString("FAILED %1").arg(exitcode));
    }

    ui->labelStatus->setVisible(true);
    ui->progressBarDownload->setVisible(false);
    ui->progressBarInstall->setVisible(false);
    ui->buttonCancel->setVisible(false);
    ui->buttonDownload->setVisible(true);
}

WtFeedPostWidget::~WtFeedPostWidget()
{
    qDeleteAll(m_imagewidgets.begin(), m_imagewidgets.end());
    m_imagewidgets.clear();
    delete ui;
}
