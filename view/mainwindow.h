#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QMessageBox>
#include <QTimer>
#include "controller/wttelemetry.h"
#include "model/DTO/wtindicators.h"
#include "service/wtfeedmapper.h"
#include "service/settings.h"
#include "DAO/wtfeeddao.h"
#include "model/wtfeedunloggedrequest.h"
#include "DAO/wtfeedimagedao.h"
#include "view/wtfeedauthorwidget.h"
#include "view/wtfeedimagewidget.h"
#include "view/wtfeedpostwidget.h"
#include "view/layout/qflowlayout.h"
#include "model/wtfeedunloggedrequest.h"
#include "model/wtfeedenums.h"
#include "initialsetupwindow.h"
#include <QInputDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Settings *settings, QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Settings *m_settings;
    WtTelemetry* warthunderAPI = nullptr;
    QTimer* apiTimer = nullptr;
    QString currentVehicle = "";
    WtFeedDAO* m_dao = nullptr;
    int m_currentpage = 0;
    QList<WtFeedPostWidget*> m_postWidgets;
    QLayout* m_postlayout = nullptr;
    InitialSetupWindow* m_setup = nullptr;
    QLabel *m_wtlabel;
    QLabel *m_7zlabel;

    void initConnect();
    void initUI();
    void fillContentTypes();
    void fillPeriod();
    void fillSort();
    void fillComboBoxes();

private slots:
    void searchAircraft();
    void apiDataReceived(bool success,QString contentType,QString json);
    void updateApiData();
    void getFeed();
    void feedReceived(QByteArray json,QNetworkReply* reply);
    void nextPage();
    void previousPage();
    void downloadRequested(WtFeedPost* post);
    void newSearch();
    void openSettings();
    void closedSettings(bool ok);
};
#endif // MAINWINDOW_H
