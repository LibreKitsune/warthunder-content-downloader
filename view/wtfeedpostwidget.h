#ifndef WTFEEDPOSTWIDGET_H
#define WTFEEDPOSTWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QSettings>
#include <QMessageBox>
#include "model/DTO/wtfeedpost.h"
#include "service/wtfeedfiledownloader.h"
#include "service/wtfeedinstaller.h"
#include "service/settings.h"
#include "wtfeedauthorwidget.h"
#include "wtfeedimagewidget.h"

namespace Ui {
class WtFeedPostWidget;
}

class WtFeedPostWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WtFeedPostWidget(WtFeedPost* post, Settings *settings, QWidget *parent = nullptr);
    ~WtFeedPostWidget();

private:
    Ui::WtFeedPostWidget *ui;
    WtFeedPost* m_post = nullptr;
    WtFeedAuthorWidget* m_authorwidget = nullptr;
    QList<QWidget*> m_imagewidgets;
    QTimer* m_imageTimer = nullptr;
    WtFeedFileDownloader* m_downloader = nullptr;
    WtFeedInstaller* m_installer = nullptr;
    Settings *m_settings;

    void initConnect();
    void initUI();

private slots:
    void nextImage();
    void previousImage();
    void downloadButtonClicked();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void installProgress(int progress);
    void installFinished(int exitcode, bool copied);
    void downloadFinished(QString path, int status);
    void installError(QString message);
    void downloadError(QString message);

signals:
    void downloadRequested(WtFeedPost *post);

};

#endif // WTFEEDPOSTWIDGET_H
