#include "wtfeedimagewidget.h"
#include "ui_wtfeedimagewidget.h"

WtFeedImageWidget::WtFeedImageWidget(WtFeedImage* image, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WtFeedImageWidget)
{
    ui->setupUi(this);

    m_image = image;
    m_dao = new WtFeedImageDAO(this);

    m_dao->getImage(m_image->src());

    connect(m_dao, &WtFeedImageDAO::responseReceived, this, &WtFeedImageWidget::imageReceived);

}

void WtFeedImageWidget::imageReceived(QPixmap img)
{
    ui->labelImage->setPixmap(img);
}

WtFeedImageWidget::~WtFeedImageWidget()
{
    delete ui;
}
