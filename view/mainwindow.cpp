#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(Settings *settings, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      m_settings(settings)
{
    ui->setupUi(this);

    m_dao = new WtFeedDAO(this);
    warthunderAPI = new WtTelemetry(this);
    apiTimer = new QTimer(this);

    m_postlayout = new FlowLayout(ui->scrollAreaWidgetContents);

    m_wtlabel = new QLabel(this);
    m_7zlabel = new QLabel(this);

    ui->statusbar->addPermanentWidget(m_wtlabel);
    ui->statusbar->addPermanentWidget(m_7zlabel);
    m_setup = new InitialSetupWindow(m_settings,false);

    initUI();
    initConnect();

    apiTimer->start(500);
    getFeed();
}

void MainWindow::initUI()
{
    fillComboBoxes();

    m_wtlabel->setText(QString("Warthunder Path: '%1'").arg(m_settings->warthunderFolder()));
    m_7zlabel->setText(QString("7zip Path: '%1'").arg(m_settings->p7zipBin()));

    ui->lineEditSearch->setFocus();
    ui->labelCurrentPage->setText(QString("Page %1").arg(m_currentpage+1));
    ui->PB_CurrentAircraft->setVisible(false);
}

void MainWindow::fillContentTypes()
{
    ui->cb_contentTypes->clear();
    ui->cb_contentTypes->addItem("All Posts",WtFeedEnums::Content::all);
    ui->cb_contentTypes->addItem("Images",WtFeedEnums::Content::image);
    ui->cb_contentTypes->addItem("Videos",WtFeedEnums::Content::video);
    ui->cb_contentTypes->addItem("Camouflages",WtFeedEnums::Content::camouflage);
    ui->cb_contentTypes->addItem("Sights",WtFeedEnums::Content::sight);
    ui->cb_contentTypes->addItem("Missions",WtFeedEnums::Content::mission);
    ui->cb_contentTypes->addItem("Locations",WtFeedEnums::Content::location);
    ui->cb_contentTypes->addItem("Models",WtFeedEnums::Content::model);
    ui->cb_contentTypes->addItem("Sounds",WtFeedEnums::Content::sound);
}

void MainWindow::initConnect()
{
    connect(ui->PB_CurrentAircraft, &QPushButton::clicked,              this,   &MainWindow::searchAircraft);
    connect(warthunderAPI,          &WtTelemetry::receivedIndicators,   this,   &MainWindow::apiDataReceived);
    connect(apiTimer,               &QTimer::timeout,                   this,   &MainWindow::updateApiData);
    connect(ui->pb_search,          &QPushButton::clicked,              this,   &MainWindow::newSearch);
    connect(m_dao,                  &WtFeedDAO::responseReceived,       this,   &MainWindow::feedReceived);
    connect(ui->pb_next,            &QPushButton::clicked,              this,   &MainWindow::nextPage);
    connect(ui->pb_prev,            &QPushButton::clicked,              this,   &MainWindow::previousPage);
    connect(ui->lineEditSearch,     &QLineEdit::returnPressed,          this,   &MainWindow::newSearch);
    connect(ui->cb_contentTypes,    &QComboBox::currentTextChanged,     this,   &MainWindow::newSearch);
    connect(ui->cb_period,          &QComboBox::currentTextChanged,     this,   &MainWindow::newSearch);
    connect(ui->cb_sort,            &QComboBox::currentTextChanged,     this,   &MainWindow::newSearch);
    connect(ui->actionSettings,     &QAction::triggered,                this,   &MainWindow::openSettings);
    connect(m_setup,                &InitialSetupWindow::closed,        this,   &MainWindow::closedSettings);
}

void MainWindow::newSearch()
{
    m_currentpage = 0;
    ui->labelCurrentPage->setText(QString("Page %1").arg(m_currentpage+1));
    getFeed();
}

void MainWindow::fillPeriod()
{
    ui->cb_period->clear();
    ui->cb_period->addItem("Past Week",WtFeedEnums::Period::pastweek);
    ui->cb_period->addItem("All Time",WtFeedEnums::Period::alltime);
    ui->cb_period->addItem("Pasth Month",WtFeedEnums::Period::pastmonth);
}

void MainWindow::fillSort()
{
    ui->cb_sort->clear();
    ui->cb_sort->addItem("Most Liked", WtFeedEnums::Sort::rating);
    ui->cb_sort->addItem("Most Commented", WtFeedEnums::Sort::comments);
    ui->cb_sort->addItem("Most Recent", WtFeedEnums::Sort::created);
    ui->cb_sort->addItem("Most Downloaded", WtFeedEnums::Sort::downloads);
}

void MainWindow::fillComboBoxes()
{
    fillContentTypes();
    fillPeriod();
    fillSort();
}

void MainWindow::downloadRequested(WtFeedPost* post)
{
    //get filename
    QString filename = "";
    if (post->file() != nullptr)
    {
         filename = post->file()->name();
    }
}


void MainWindow::searchAircraft()
{
    if (currentVehicle != "" && currentVehicle != "INVALID")
    {
        QString type = currentVehicle;
        ui->lineEditSearch->setText(type);
        getFeed();
    }
}

void MainWindow::apiDataReceived(bool success,QString contentType,QString json)
{
    WtIndicators* indicators = nullptr;
    if (success && contentType == "application/json")
    {
        indicators = WtIndicators::fromJson(json);
        if (indicators->getValid())
        {
            currentVehicle = indicators->getType();

        }
        else
        {
            currentVehicle = "INVALID";
        }

        ui->PB_CurrentAircraft->setText(QString("Search current Aircraft: %1").arg(currentVehicle));
    }

    if (indicators != nullptr)
    {
        delete indicators;
    }
}

void MainWindow::updateApiData()
{
    warthunderAPI->getIndicators();
}

void MainWindow::getFeed()
{
    WtFeedUnloggedRequest request;
    request.setContent(ui->cb_contentTypes->currentData().toInt());
    request.setPeriod(ui->cb_period->currentData().toInt());
    request.setSort(ui->cb_sort->currentData().toInt());
    request.setSearch(ui->lineEditSearch->text());
    request.setPage(m_currentpage);
    m_dao->getUnlogged(&request);
}

void MainWindow::feedReceived(QByteArray json, QNetworkReply* reply)
{
    // Declare reply as unused to avoid compiler warnings
    Q_UNUSED(reply);

    // Scroll back to top
    ui->scrollArea->ensureVisible(0,0);


    // Remove old widgets
    foreach(WtFeedPostWidget* fpw, m_postWidgets)
    {
        disconnect(fpw, &WtFeedPostWidget::downloadRequested,this, &MainWindow::downloadRequested);
        m_postlayout->removeWidget(fpw);
    }
    qDeleteAll(m_postWidgets.begin(), m_postWidgets.end());
    m_postWidgets.clear();


    // create new widgets from result
    WtFeedMapper mapper;
    WtFeedResponse* res = mapper.createResponseFromJSON(json);    
    foreach (WtFeedPost* post, res->posts())
    {
        WtFeedPostWidget* fpw = new WtFeedPostWidget(post,m_settings, this);
        m_postWidgets.append(fpw);
        m_postlayout->addWidget(fpw);
        connect(fpw, &WtFeedPostWidget::downloadRequested,this, &MainWindow::downloadRequested);
    }
    delete res;
}

void MainWindow::nextPage()
{
    m_currentpage++;
    getFeed();
    ui->labelCurrentPage->setText(QString("Page %1").arg(m_currentpage+1));
}

void MainWindow::previousPage()
{
    m_currentpage--;
    if (m_currentpage < 0)
    {
        m_currentpage = 0;
    }
    getFeed();
    ui->labelCurrentPage->setText(QString("Page %1").arg(m_currentpage+1));
}

void MainWindow::openSettings()
{
    m_setup->show();
}

void MainWindow::closedSettings(bool ok)
{
    if (ok)
    {
        QMessageBox::information(this,"Settings","Changes to the theme will be applied after a restart");
        initUI();
        getFeed();
    }
}

MainWindow::~MainWindow()
{
    delete m_setup;
    delete ui;
}

