#include "view/mainwindow.h"
#include "view/initialsetupwindow.h"
#include "service/settings.h"
#include <QApplication>
#include <QSettings>
#include "tools/version.h"
#include <QFileInfo>
#include <QCommandLineParser>
#include <QStyleFactory>

void log(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString name = QString("%1.log").arg(QCoreApplication::applicationName());
    QString outfile = QString("%1/%2").arg(path,name);

    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";

    QByteArray localMsg = msg.toLocal8Bit();

    Settings settings;

    QString output = "%1: %2 \n";
    QString typeStr = "Info:";

    switch (type) {
        case QtDebugMsg:
            typeStr = "DEBUG";            
            break;
        case QtInfoMsg:
            typeStr = "INFO";
            break;
        case QtWarningMsg:
            typeStr = "WARNING";
            break;
        case QtCriticalMsg:
            typeStr = "CRITICAL";
            break;
        case QtFatalMsg:
            typeStr = "FATAL";
            break;
        }

    if (settings.logstdout())
    {
        fprintf(stderr, "%s: %s (%s:%u, %s)\n", typeStr.toLocal8Bit().constData() , localMsg.constData(), file, context.line, function);
    }


    if (settings.writeAppLog())
    {
        if (settings.logstdout())
        {
            fprintf(stderr,"Info: Opening logfile %s\n", outfile.toLocal8Bit().constData());
        }

        QFile logfile(outfile);
        if (logfile.open(QFile::Append | QFile::Text))
        {
            logfile.write(output.arg(typeStr,localMsg).toLocal8Bit());
            logfile.close();
        }
    }

}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(log);
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("oesterle.dev");
    QCoreApplication::setOrganizationDomain("oesterle.dev");
    QCoreApplication::setApplicationName("wt-content-downloader");
    QCoreApplication::setApplicationVersion(VERSION.toString());

    Settings settings;
#ifdef Q_OS_WIN    
    qApp->setStyle(QStyleFactory::create("Fusion"));



#endif

    if(settings.appDarkTheme()){
        QPalette darkPalette;
        QColor darkColor = QColor(45,45,45);
        QColor disabledColor = QColor(127,127,127);
        darkPalette.setColor(QPalette::Window, darkColor);
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, QColor(18,18,18));
        darkPalette.setColor(QPalette::AlternateBase, darkColor);
        darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Disabled, QPalette::Text, disabledColor);
        darkPalette.setColor(QPalette::Button, darkColor);
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, disabledColor);
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

        darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        darkPalette.setColor(QPalette::Disabled, QPalette::HighlightedText, disabledColor);

        qApp->setPalette(darkPalette);

        qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
    }


    QCommandLineParser parser;
    parser.addVersionOption();
    parser.process(a);


    bool setupHasBeenRun = settings.setupHasBeenRun();

    QWidget* w_ptr;

    if (!setupHasBeenRun)
    {
        w_ptr = new InitialSetupWindow(&settings, true);
    }
    else
    {
        w_ptr = new MainWindow(&settings);
    }

    w_ptr->show();
    int val =  a.exec();

    delete w_ptr;    
    return val;
}
