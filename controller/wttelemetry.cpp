#include "wttelemetry.h"

WtTelemetry::WtTelemetry(QObject *parent) : QObject(parent)
{
    network = new QNetworkAccessManager(this);
    connect(network,SIGNAL(finished(QNetworkReply*)),this,SLOT(sReceivedIndicators(QNetworkReply*)));
}

WtTelemetry::~WtTelemetry()
{
    if (network != nullptr)
    {
        delete network;
    }
}

void WtTelemetry::getIndicators()
{
    QUrl indicatorUrl(URL_INDICATORS);
    QNetworkRequest request(indicatorUrl);
    network->get(request);

}

void WtTelemetry::sReceivedIndicators(QNetworkReply *reply)
{
    bool success = false;
    QString contentType = "text/plain";
    QString content = "";
    if (reply->error())
    {
        success = false;
        contentType = "text/plain";
        content = reply->errorString();
    }
    else
    {
        success = true;
        contentType = reply->header(QNetworkRequest::ContentTypeHeader).toString();
        content = reply->readAll();
    }

    emit(receivedIndicators(success,contentType,content));
}
