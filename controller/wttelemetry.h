#ifndef WTTELEMETRY_H
#define WTTELEMETRY_H
#include <QString>
#include <QObject>
#include <QImage>
#include <QNetworkAccessManager>
#include <QNetworkReply>
class WtTelemetry : public QObject
{
    Q_OBJECT
    public:
        explicit WtTelemetry(QObject *parent = nullptr);
        ~WtTelemetry();
        QNetworkAccessManager* network = nullptr;
        const QString IP_ADDRESS = "127.0.0.1:8111";
        const QString URL_INDICATORS = "http://"+IP_ADDRESS+"/indicators";
        const QString URL_STATE = "http://"+IP_ADDRESS+"/state";
        const QString URL_MAP = "http://"+IP_ADDRESS+"/map.img";
        const QString URL_MAP_OBJ = "http://"+IP_ADDRESS+"/map_obj.json";
        const QString URL_MAP_INFO = "http://"+IP_ADDRESS+"/map_info.json";

        void getIndicators();

    private slots:
        void sReceivedIndicators(QNetworkReply *reply);

    signals:
        void receivedIndicators(bool success,QString contentType,QString json);
        void receivedState(QString json);
        void receivedMap(QImage image);
        void receivedMapObj(QString json);
        void receivedMapInfo(QString json);
};

#endif // WTTELEMETRY_H
