#include "wtfeedenums.h"

WtFeedEnums::WtFeedEnums()
{

}

QString WtFeedEnums::contentToKeyString(int content)
{
    return contentToKeyString(static_cast<Content>(content));
}

QString WtFeedEnums::contentToKeyString(Content content)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Content>();
    return metaEnum.valueToKey(content);
}

QString WtFeedEnums::sortToKeyString(Sort sort)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Sort>();
    return metaEnum.valueToKey(sort);
}

WtFeedEnums::Content WtFeedEnums::stringKeyToContent(QString content)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Content>();
    int value = metaEnum.keyToValue(content.toStdString().c_str());
    return static_cast<Content>(value);
}

WtFeedEnums::Content WtFeedEnums::stringToContent(QString content)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<Content>();

    for (int i = 0; i < metaEnum.keyCount(); i++)
    {
        const char* s = metaEnum.key(i); // enum name as string
        int v = metaEnum.value(i); // enum value
        QString name = QString(s);

        if (name.toUpper() == content.toUpper())
        {
            return static_cast<Content>(v);
        }
    }

    return all;
}
