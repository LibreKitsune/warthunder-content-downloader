#ifndef WTFEEDUNLOGGEDREQUEST_H
#define WTFEEDUNLOGGEDREQUEST_H
#include <QString>
#include <QObject>
#include "wtfeedenums.h"
class WtFeedUnloggedRequest : public QObject
{
    Q_OBJECT

public:
    explicit WtFeedUnloggedRequest(QObject *parent = nullptr);

    QString endpoint();
    QString content();
    QString sort();
    QString user();
    int period();
    int page();
    int featured();
    QString subtype();
    QString search();

    void setContent(int content);
    void setContent(WtFeedEnums::Content content);
    void setSort(WtFeedEnums::Sort sort);
    void setSort(int sort);
    void setUser(QString user);
    void setPeriod(WtFeedEnums::Period period);
    void setPeriod(int period);
    void setPage(int page);
    void setFeatured(int featured);
    void setSubtype(WtFeedEnums::Content subtype);
    void setSubtype(int subtype);
    void setSearch(QString search);

private:
    const QString m_url = "/api/feed/get_unlogged/";
    WtFeedEnums::Content m_content = WtFeedEnums::all;
    WtFeedEnums::Sort m_sort = WtFeedEnums::created;
    QString m_user = "";
    int m_period = WtFeedEnums::alltime;
    int m_page = 0;
    int m_featured = 0;
    WtFeedEnums::Content m_subtype = WtFeedEnums::all;
    QString m_search = "";
};

#endif // WTFEEDUNLOGGEDREQUEST_H
