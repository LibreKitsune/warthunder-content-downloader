#ifndef WTFEEDENUMS_H
#define WTFEEDENUMS_H
#include <QObject>
#include <QMetaEnum>
class WtFeedEnums
{
    Q_GADGET
public:
    WtFeedEnums();

    enum Content {all, image, video, camouflage, sight, mission, location, model, sound};
    enum Sort {created, rating, comments, downloads};
    enum Period {alltime=0, pastweek=7, pastmonth=30};

    Q_ENUM(Content)
    Q_ENUM(Sort)
    Q_ENUM(Period)

    static QString contentToKeyString(Content content);
    static QString contentToKeyString(int content);
    static QString sortToKeyString(Sort sort);

    static Content stringKeyToContent(QString content);
    static Content stringToContent(QString content);
};

#endif // WTFEEDENUMS_H
