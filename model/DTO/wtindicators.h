#ifndef WTINDICATORS_H
#define WTINDICATORS_H
#include <QObject>
#include <QString>
#include <QMap>
#include <QVariant>
#include <QVariantMap>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
class WtIndicators : public QObject
{
public:
    explicit WtIndicators(QObject *parent = nullptr);
    static WtIndicators* fromJson(QString json);
    bool getValid();
    QString getType();
    QVariantMap getAttributes();
    QMap<QString, QVariant>::iterator getAttribute(QString key);
    void setType(QString type);
    void setValid(bool valid);
    void setAttributes(QVariantMap attributes);

private:
    bool pValid;
    QString pType;
    QVariantMap pAttributes;
};

#endif // WTINDICATORS_H
