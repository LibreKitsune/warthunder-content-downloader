#include "wtfeedimage.h"

WtFeedImage::WtFeedImage(QObject *parent) : QObject(parent)
{

}

int WtFeedImage::ID()
{
    return m_id;
}

QMimeType WtFeedImage::type()
{
    return m_type;
}

QUrl WtFeedImage::src()
{
    return m_src;
}

QSize WtFeedImage::size()
{
    return m_size;
}

qreal WtFeedImage::ratio()
{
    return m_ratio;
}

void WtFeedImage::setID(int id)
{
    m_id = id;
}

void WtFeedImage::setType(QMimeType type)
{
    m_type = QMimeType(type);
}

void WtFeedImage::setSrc(QUrl url)
{
    m_src = QUrl(url);
}

void WtFeedImage::setSize(QSize size)
{
    m_size = QSize(size);
}

void WtFeedImage::setRatio(qreal ratio)
{
    m_ratio = ratio;
}
