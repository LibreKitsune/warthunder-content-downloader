#include "wtindicators.h"

WtIndicators::WtIndicators(QObject *parent) : QObject(parent)
{

}

WtIndicators* WtIndicators::fromJson(QString json)
{
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toUtf8());
    QJsonObject jsonObject = jsonDocument.object();

    QJsonValue jsonValueValid = jsonObject.value(QString("valid"));
    QJsonValue jsonValueType = jsonObject.value(QString("type"));

    QVariantMap values = jsonObject.toVariantMap();
    bool valid = jsonValueValid.toBool(false);
    QString type = jsonValueType.toString("");

    WtIndicators* obj = new WtIndicators();

    obj->setAttributes(values);
    obj->setValid(valid);
    obj->setType(type);

    return obj;
}

QVariantMap WtIndicators::getAttributes()
{
    return pAttributes;
}

QMap<QString, QVariant>::iterator WtIndicators::getAttribute(QString key)
{

    return pAttributes.find(key);
}

void WtIndicators::setAttributes(QVariantMap attributes)
{
    pAttributes = attributes;
}

QString WtIndicators::getType()
{
    return pType;
}

void WtIndicators::setType(QString type)
{
    pType = type;
}

bool WtIndicators::getValid()
{
    return pValid;
}

void WtIndicators::setValid(bool valid)
{
    pValid = valid;
}
