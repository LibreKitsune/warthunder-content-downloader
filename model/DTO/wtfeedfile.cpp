#include "wtfeedfile.h"

WtFeedFile::WtFeedFile(QObject *parent) : QObject(parent)
{

}

int WtFeedFile::ID()
{
    return m_id;
}

QString WtFeedFile::name()
{
    return m_name;
}

QUrl WtFeedFile::link()
{
    return m_link;
}

QMimeType WtFeedFile::type()
{
    return m_type;
}

int WtFeedFile::size()
{
    return m_size;
}

void WtFeedFile::setID(int id)
{
    m_id = id;
}

void WtFeedFile::setName(QString name)
{
    m_name = QString(name);
}

void WtFeedFile::setLink(QUrl url)
{
    m_link = QUrl(url);
}

void WtFeedFile::setType(QMimeType type)
{
    m_type = QMimeType(type);
}

void WtFeedFile::setSize(int size)
{
    m_size = size;
}
