#ifndef WTFEEDPOST_H
#define WTFEEDPOST_H

#include <QObject>
#include <QDateTime>
#include "wtfeedimage.h"
#include "wtfeedfile.h"
#include "wtfeedauthor.h"
#include "../wtfeedenums.h"

class WtFeedPost : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedPost(QObject *parent = nullptr);

    int ID();
    int langGroup();
    QString language();
    WtFeedEnums::Content type();
    QDateTime created();
    bool isAuthor();
    bool visible();
    bool isSpecial();
    WtFeedAuthor* author();
    int likes();
    bool isLiked();
    int views();
    bool doubt();
    bool featured();
    int downloads();
    int comments();
    bool isPinned();
    bool isMarketSuitable();
    bool canDelete();
    bool canEdit();
    QString description();
    QList<WtFeedImage*> images();
    WtFeedFile* file();
    bool pbrReady();

    void setID(int id);
    void setLangGroup(int langGroup);
    void setLanguage(QString lang);
    void setType(WtFeedEnums::Content type);
    void setCreated(QDateTime created);
    void setIsAuthor(bool isAuthor);
    void setVisible(bool visible);
    void setIsSpecial(bool isSpecial);
    void setAuthor(WtFeedAuthor *author);
    void setLikes(int likes);
    void setIsLiked(bool isLiked);
    void setViews(int views);
    void setDoubt(bool doubt);
    void setFeatured(bool featured);
    void setDownloads(int downloads);
    void setComments(int comments);
    void setIsPinned(bool isPinned);
    void setIsMarketSuitable(bool isSuitable);
    void setCanDelete(bool canDelete);
    void setCanEdit(bool canEdit);
    void setDescription(QString description);
    void addImage(WtFeedImage *image);
    void setFile(WtFeedFile *file);
    void setPbrReady(bool ready);

private:
    int m_langGroup;
    int m_id;
    QString m_language;
    WtFeedEnums::Content m_type;
    QDateTime m_created;
    bool m_isAuthor;
    bool m_visible;
    bool m_isSpecial;
    int m_likes;
    bool m_isLiked;
    int m_views;
    bool m_doubt;
    bool m_featured;
    int m_downloads;
    int m_comments;
    bool m_isPinned;
    bool m_isMarketSuitable;
    bool m_canDelete;
    bool m_canEdit;
    QString m_description;
    bool m_pbrReady;
    QList<WtFeedImage*> m_images;
    WtFeedFile *m_file = nullptr;
    WtFeedAuthor *m_author = nullptr;

signals:

};

#endif // WTFEEDPOST_H
