#include "wtfeedresponse.h"

WtFeedResponse::WtFeedResponse(QObject *parent) : QObject(parent)
{

}

QString WtFeedResponse::status()
{
    return m_status;
}

QString WtFeedResponse::pageTitle()
{
    return m_pageTitle;
}

QUrl WtFeedResponse::link()
{
    return m_link;
}

QList<WtFeedPost*> WtFeedResponse::posts()
{
    return m_posts;
}

void WtFeedResponse::setStatus(QString status)
{
    m_status = QString(status);
}

void WtFeedResponse::setPageTitle(QString title)
{
    m_pageTitle = QString(title);
}

void WtFeedResponse::setLink(QUrl link)
{
    m_link = QUrl(link);
}

void WtFeedResponse::addPost(WtFeedPost *post)
{
    post->setParent(this);
    m_posts.append(post);
}
