#ifndef WTFEEDAUTHOR_H
#define WTFEEDAUTHOR_H

#include <QObject>
#include <QUrl>
class WtFeedAuthor : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedAuthor(QObject *parent = nullptr);

    int ID();
    QString nickname();
    QUrl avatarUrl();

    void setID(int id);
    void setNickname(QString nickname);
    void setAvatarUrl(QUrl url);

private:
    int m_id;
    QString m_nickname;
    QUrl m_avatar;

signals:

};

#endif // WTFEEDAUTHOR_H
