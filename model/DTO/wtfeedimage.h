#ifndef WTFEEDIMAGE_H
#define WTFEEDIMAGE_H

#include <QObject>
#include <QMimeType>
#include <QSize>
#include <QUrl>
class WtFeedImage : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedImage(QObject *parent = nullptr);

    int ID();
    QMimeType type();
    QUrl src();
    QSize size();
    qreal ratio();

    void setID(int id);
    void setType(QMimeType type);
    void setSrc(QUrl url);
    void setSize(QSize size);
    void setRatio(qreal ratio);

private:
    int m_id;
    QMimeType m_type;
    QUrl m_src;
    QSize m_size;
    qreal m_ratio;

signals:

};

#endif // WTFEEDIMAGE_H
