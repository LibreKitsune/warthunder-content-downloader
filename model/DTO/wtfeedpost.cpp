#include "wtfeedpost.h"

WtFeedPost::WtFeedPost(QObject *parent) : QObject(parent)
{

}

int WtFeedPost::ID()
{
    return m_id;
}

int WtFeedPost::langGroup()
{
    return m_langGroup;
}

QString WtFeedPost::language()
{
    return m_language;
}

WtFeedEnums::Content WtFeedPost::type()
{
    return m_type;
}

QDateTime WtFeedPost::created()
{
    return m_created;
}

bool WtFeedPost::isAuthor()
{
    return m_isAuthor;
}

bool WtFeedPost::visible()
{
    return m_visible;
}

bool WtFeedPost::isSpecial()
{
    return m_isSpecial;
}

WtFeedAuthor* WtFeedPost::author()
{
    return m_author;
}

int WtFeedPost::likes()
{
    return m_likes;
}

bool WtFeedPost::isLiked()
{
    return m_isLiked;
}

int WtFeedPost::views()
{
    return m_views;
}

bool WtFeedPost::doubt()
{
    return m_doubt;
}

bool WtFeedPost::featured()
{
    return m_featured;
}

int WtFeedPost::downloads()
{
    return m_downloads;
}

int WtFeedPost::comments()
{
    return m_comments;
}

bool WtFeedPost::isPinned()
{
    return m_isPinned;
}

bool WtFeedPost::isMarketSuitable()
{
    return m_isMarketSuitable;
}

bool WtFeedPost::canDelete()
{
    return m_canDelete;
}

bool WtFeedPost::canEdit()
{
    return m_canEdit;
}

QString WtFeedPost::description()
{
    return m_description;
}

QList<WtFeedImage*> WtFeedPost::images()
{
    return m_images;
}

WtFeedFile* WtFeedPost::file()
{
    return m_file;
}

bool WtFeedPost::pbrReady()
{
    return m_pbrReady;
}

void WtFeedPost::setID(int id)
{
    m_id = id;
}

void WtFeedPost::setLangGroup(int langGroup)
{
    m_langGroup = langGroup;
}

void WtFeedPost::setLanguage(QString lang)
{
    m_language = QString(lang);
}

void WtFeedPost::setType(WtFeedEnums::Content type)
{
    m_type = type;
}

void WtFeedPost::setCreated(QDateTime created)
{
    m_created = QDateTime(created);
}

void WtFeedPost::setIsAuthor(bool isAuthor)
{
    m_isAuthor = isAuthor;
}

void WtFeedPost::setVisible(bool visible)
{
    m_visible = visible;
}

void WtFeedPost::setIsSpecial(bool isSpecial)
{
    m_isSpecial = isSpecial;
}

void WtFeedPost::setAuthor(WtFeedAuthor *author)
{
    author->setParent(this);
    m_author = author;
}

void WtFeedPost::setLikes(int likes)
{
    m_likes = likes;
}

void WtFeedPost::setIsLiked(bool isLiked)
{
    m_isLiked = isLiked;
}

void WtFeedPost::setViews(int views)
{
    m_views = views;
}

void WtFeedPost::setDoubt(bool doubt)
{
    m_doubt = doubt;
}

void WtFeedPost::setFeatured(bool featured)
{
    m_featured = featured;
}

void WtFeedPost::setDownloads(int downloads)
{
    m_downloads = downloads;
}

void WtFeedPost::setComments(int comments)
{
    m_comments = comments;
}

void WtFeedPost::setIsPinned(bool isPinned)
{
    m_isPinned = isPinned;
}

void WtFeedPost::setIsMarketSuitable(bool isSuitable)
{
    m_isMarketSuitable = isSuitable;
}

void WtFeedPost::setCanDelete(bool canDelete)
{
    m_canDelete = canDelete;
}

void WtFeedPost::setCanEdit(bool canEdit)
{
    m_canEdit = canEdit;
}

void WtFeedPost::setDescription(QString description)
{
    m_description = QString(description);
}

void WtFeedPost::addImage(WtFeedImage *image)
{
    image->setParent(this);
    m_images.append(image);
}

void WtFeedPost::setFile(WtFeedFile *file)
{
    file->setParent(this);
    m_file = file;
}

void WtFeedPost::setPbrReady(bool ready)
{
    m_pbrReady = ready;
}
