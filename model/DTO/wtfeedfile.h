#ifndef WTFEEDFILE_H
#define WTFEEDFILE_H

#include <QObject>
#include <QUrl>
#include <QMimeType>

class WtFeedFile : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedFile(QObject *parent = nullptr);

    int ID();
    QString name();
    QUrl link();
    QMimeType type();
    int size();

    void setID(int id);
    void setName(QString name);
    void setLink(QUrl url);
    void setType(QMimeType type);
    void setSize(int size);

private:
   int m_id;
   QString m_name;
   QUrl m_link;
   QMimeType m_type;
   int m_size;


signals:

};

#endif // WTFEEDFILE_H
