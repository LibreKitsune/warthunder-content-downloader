#include "wtfeedauthor.h"

WtFeedAuthor::WtFeedAuthor(QObject *parent) : QObject(parent)
{

}

int WtFeedAuthor::ID()
{
    return m_id;
}

QString WtFeedAuthor::nickname()
{
    return m_nickname;
}

QUrl WtFeedAuthor::avatarUrl()
{
    return m_avatar;
}

void WtFeedAuthor::setID(int id)
{
    m_id = id;
}

void WtFeedAuthor::setNickname(QString nickname)
{
    m_nickname = QString(nickname);
}

void WtFeedAuthor::setAvatarUrl(QUrl url)
{
    m_avatar = QUrl(url);
}
