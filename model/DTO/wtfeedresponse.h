#ifndef WTFEEDRESPONSE_H
#define WTFEEDRESPONSE_H

#include <QObject>
#include <QUrl>
#include "wtfeedpost.h"
class WtFeedResponse : public QObject
{
    Q_OBJECT
public:
    explicit WtFeedResponse(QObject *parent = nullptr);

    QString status();
    QString pageTitle();
    QUrl link();
    QList<WtFeedPost*> posts();

    void setStatus(QString status);
    void setPageTitle(QString title);
    void setLink(QUrl link);
    void addPost(WtFeedPost *post);

private:
    QString m_status;
    QString m_pageTitle;
    QUrl m_link;
    QList<WtFeedPost*> m_posts;

signals:

};

#endif // WTFEEDRESPONSE_H
