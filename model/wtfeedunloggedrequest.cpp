#include "wtfeedunloggedrequest.h"

WtFeedUnloggedRequest::WtFeedUnloggedRequest(QObject *parent) : QObject(parent)
{

}

QString WtFeedUnloggedRequest::endpoint()
{
    return m_url;
}

QString WtFeedUnloggedRequest::content()
{
    return WtFeedEnums::contentToKeyString(m_content);
}

QString WtFeedUnloggedRequest::sort()
{
    return WtFeedEnums::sortToKeyString(m_sort);
}

QString WtFeedUnloggedRequest::user()
{
    return m_user;
}

int WtFeedUnloggedRequest::period()
{
    return m_period;
}

int WtFeedUnloggedRequest::page()
{
    return m_page;
}

int WtFeedUnloggedRequest::featured()
{
    return m_featured;
}

QString WtFeedUnloggedRequest::subtype()
{
    return WtFeedEnums::contentToKeyString(m_subtype);
}

QString WtFeedUnloggedRequest::search()
{
    return m_search;
}

void WtFeedUnloggedRequest::setContent(int content)
{
    m_content = static_cast<WtFeedEnums::Content>(content);
}

void WtFeedUnloggedRequest::setContent(WtFeedEnums::Content content)
{
    m_content = content;
}

void WtFeedUnloggedRequest::setSort(WtFeedEnums::Sort sort)
{
    m_sort = sort;
}

void WtFeedUnloggedRequest::setSort(int sort)
{
    m_sort = static_cast<WtFeedEnums::Sort>(sort);
}

void WtFeedUnloggedRequest::setUser(QString user)
{
    m_user = user;
}

void WtFeedUnloggedRequest::setPeriod(WtFeedEnums::Period period)
{
    m_period = period;
}

void WtFeedUnloggedRequest::setPeriod(int period)
{
    m_period = period;
}

void WtFeedUnloggedRequest::setPage(int page)
{
    m_page = page;
}

void WtFeedUnloggedRequest::setFeatured(int featured)
{
    m_featured = featured;
}

void WtFeedUnloggedRequest::setSubtype(WtFeedEnums::Content subtype)
{
    m_subtype = subtype;
}

void WtFeedUnloggedRequest::setSubtype(int subtype)
{
    m_subtype = static_cast<WtFeedEnums::Content>(subtype);
}

void WtFeedUnloggedRequest::setSearch(QString search)
{
    if (search.length() > 1)
    {
        m_search = QString("#%1").arg(search);
    }
    else
    {
        m_search = "";
    }
}
