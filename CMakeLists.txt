cmake_minimum_required(VERSION 3.1.0)

project(wt-content-downloader LANGUAGES CXX)

find_package(Qt6 COMPONENTS Widgets REQUIRED)
find_package(Qt6 COMPONENTS Network REQUIRED)
include(version.cmake)
include(package.cmake)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

if (WIN32)
    set(CMAKE_CXX_FLAGS "-mwindows")
endif (WIN32)

set(SOURCES
    DAO/wtfeeddao.cpp
    DAO/wtfeedimagedao.cpp
    DAO/wtfeedfiledao.cpp
    controller/wttelemetry.cpp
    model/DTO/wtfeedauthor.cpp
    model/DTO/wtfeedfile.cpp
    model/DTO/wtfeedimage.cpp
    model/DTO/wtfeedpost.cpp
    model/DTO/wtfeedresponse.cpp
    model/DTO/wtindicators.cpp
    model/wtfeedenums.cpp
    model/wtfeedunloggedrequest.cpp
    service/settings.cpp
    service/wtfeedfiledownloader.cpp
    service/wtfeedinstaller.cpp
    service/wtfeedmapper.cpp
    view/initialsetupwindow.cpp
    main.cpp
    view/layout/qflowlayout.cpp
    view/mainwindow.cpp
    view/wtfeedauthorwidget.cpp
    view/wtfeedimagewidget.cpp
    view/wtfeedpostwidget.cpp
    view/initialsetupwindow.ui
    view/mainwindow.ui
    view/wtfeedauthorwidget.ui
    view/wtfeedimagewidget.ui
    view/wtfeedpostwidget.ui
    wt-content-downloader_en_GB.ts
)



add_executable(${PROJECT_NAME} ${SOURCES})

target_include_directories(
    ${PROJECT_NAME}
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/DAO
        ${CMAKE_CURRENT_SOURCE_DIR}/controller
        ${CMAKE_CURRENT_SOURCE_DIR}/model/DTO
        ${CMAKE_CURRENT_SOURCE_DIR}/model
        ${CMAKE_CURRENT_SOURCE_DIR}/service
        ${CMAKE_CURRENT_SOURCE_DIR}/view
        ${CMAKE_CURRENT_SOURCE_DIR}/view/layout
        ${CMAKE_CURRENT_SOURCE_DIR}/tools
        ${CMAKE_CURRENT_SOURCE_DIR}
)

install(TARGETS ${PROJECT_NAME} DESTINATION bin)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/package/icon.ico DESTINATION share/pixmaps/wt-content-downloader/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/package/${PROJECT_NAME}.desktop DESTINATION share/applications)

target_link_libraries(${PROJECT_NAME} Qt6::Widgets Qt6::Network)
